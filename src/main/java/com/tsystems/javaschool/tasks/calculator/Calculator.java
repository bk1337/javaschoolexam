package com.tsystems.javaschool.tasks.calculator;

//import org.graalvm.compiler.hotspot.stubs.DivisionByZeroExceptionStub; This generated an error in my IDEA

import javax.security.auth.callback.CallbackHandler;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class Calculator {



    /**
     * Constructs the list of operators
     * @return LinkedList of operators
     */
    private static LinkedList<Character> setUpOperators(){
        LinkedList<Character> result = new LinkedList<Character>();
        result.add('+');
        result.add('-');
        result.add('*');
        result.add('/');
        return result;
    }


    private static LinkedList<Character> operators = setUpOperators();


    /**
     * Checks if char is a delimiter
     *
     * @param token character we check
     * @return <code>true</code> if the given character is a delimiter and <code>false</code> otherwise
     */
    private static boolean isDelimiter(Character token){
        return token == ' ';
    }

    /**
     * Checks if char is an operator
     *
     * @param token character we check
     * @return <code>true</code> if the given character is an operator and <code>false</code> otherwise
     */
    private static boolean isOperator(Character token){
        return operators.contains(token);
    }

    /**
     * Returns priority of the operation
     * @param token operation we check
     * @return int value that shows priority of the given operation
     */
    private static int getPriority(Character token){
        switch (token){
            case '-':
            case '+':
                return 1;
            case '*':
            case '/':
                return 2;
            default:
                return -1;

        }
    }

    /**
     * Makes an arithmetic calculation with two operators and pushes the result back to the list of operands
     * @param operands is the list of operands operation we check
     * @param operation is the operation we have to make
     */
    static void makeCalculation(LinkedList<Double> operands, Character operation){
        double right = operands.removeLast();
        double left = operands.removeLast();
        switch (operation) {
            case '+':
                operands.add(left+right);
                break;
            case '-':
                operands.add(left-right);
                break;
            case '*':
                operands.add(left*right);
                break;
            case '/':
                if(right == 0)
                    operands.add(null);
                else
                    operands.add(left/right);
                break;
        }
    }

    /**
     * Checks if we got null operand (for example after division by 0)
     * @param operands is the list of operands operation we check
     * @return <code>true</code> if we can continue and <code>false</code> otherwise
     */
    private boolean checkNull(LinkedList<Double> operands){
        if (operands.getLast() == null)
            return true;
        else
            return false;
    }

    /**
     * Checks if the given character is digit or dot
     * @param token is the character we check
     * @return <code>true</code> if the given character can be used to construct and <code>false</code> otherwise
     */
    private boolean isDigit(Character token){
        return Character.isDigit(token) || token=='.';
    }




    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if(statement == null || statement==""){
            return null;
        }
        else {
            LinkedList<Double> operands = new LinkedList<>();
            LinkedList<Character> operations = new LinkedList<>();
            char prev = statement.charAt(0);
            for (int i = 0; i < statement.length(); i++) {
                char c = statement.charAt(i);
                if(c == prev && i!=0)
                    return null;
                prev = c;
                if (isDelimiter(c))
                    continue;
                if (c == '(')
                    operations.add('(');
                else if (c == ')') {
                    if(!operations.contains('('))
                        return null;
                    while (operations.getLast() != '('){
                        makeCalculation(operands,operations.removeLast());
                        if(checkNull(operands))
                            return null;
                    }

                    operations.removeLast();
                } else if (isOperator(c)) {
                    while (!operations.isEmpty() && getPriority(operations.getLast()) >= getPriority(c)){
                        makeCalculation(operands, operations.removeLast());
                        if(checkNull(operands))
                            return null;
                    }
                    operations.add(c);
                } else {
                    String operand = "";
                    while (i < statement.length()){
                        if (statement.charAt(i) == ',')
                            return null;
                        else {
                            if (isDigit(statement.charAt(i))) {
                                if(statement.charAt(i) == '.' && statement.charAt(i)==operand.charAt(operand.length()-1))
                                    return null;
                                operand += statement.charAt(i++);
                            } else
                                break;
                        }
                    }
                    --i;
                    operands.add(Double.parseDouble(operand));
                }
            }
            while (!operations.isEmpty()){
                if(operations.contains('(') && !operations.contains(')'))
                    return null;
                makeCalculation(operands, operations.removeLast());
                if(checkNull(operands))
                    return null;
            }

            if(operands.get(0)%1==0){
                Integer res = operands.get(0).intValue();
                return res.toString();
            }
            else {
                return operands.get(0).toString();
            }

        }


    }
}




