package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Finds the number of rows in the pyramid
     * @param inputNumbers to be used in the pyramid
     * @return number of rows
     */
    private int findFirstDimension(List<Integer> inputNumbers){
        int count = 0;
        int numbersCount = inputNumbers.size();
        int size = 0;
        while(numbersCount>0){
            count++;
            numbersCount-=count;
            size++;
        }
        return size;
    }

    /**
     * Finds the number of columns in the pyramid
     * @param firsDimensionSize is the number of rows in pyramid
     * @return number of columns
     */
    private int findSecondDimension(int firsDimensionSize){
        return firsDimensionSize*2-1;
    }

    /**
     * Checks if the length input is enough to build a pyramid
     * @param inputNumbers is the given input
     * @return <code>true</code> if it is possible to build a pyramid and <code>false</code> otherwise
     */
    private boolean findNeededCount(List<Integer> inputNumbers){
        int size = findFirstDimension(inputNumbers);
        int count = 0;
        int neededSize = 0;
        for(int i=0;i<size;i++){
            count++;
            neededSize+=count;
        }
        return inputNumbers.size() == neededSize;
    }

    /**
     * Creates a row in a pyramid
     * @param size is the number of columns in the pyramid
     * @param numbers is a list of values we will use in this row
     * @return a row of the pyramid
     */
    private int[] CreateLevel(int size, List<Integer> numbers){
        int[] level = new int[size];
        int usefulLength = numbers.size();
        int current = 0;
        int border = (level.length - usefulLength)/2;
        for(int i=0;i<level.length;i++){
            if(i<border || i>=border + usefulLength)
                level[i]=0;
            else {
                level[i] = numbers.get(current);
                current++;
            }
        }
        return level;
    }

    /**
     * Creates a list of values for each row of the pyramid
     * @param startPos is an index in the given input where whe start to collect the list
     * @param count is number of elements in the list
     * @param inputNumbers is the given input
     * @return list of values for each row of the pyramid
     */
    private LinkedList<Integer> getNeededNumbers(int startPos, int count, List<Integer> inputNumbers){
        LinkedList<Integer> res = new LinkedList<>();
        if(count==1){
            res.add(inputNumbers.get(startPos));
            return res;
        }
        else {
            for(int i=startPos;i<startPos+count;i++){
                res.add(inputNumbers.get(i));
                res.add(0);
            }
            res.removeLast();
            return res;
        }



    }


    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {


        if(!findNeededCount(inputNumbers))//if we do not have enough elements to build a symmetrical pyramid
            throw new CannotBuildPyramidException("not enough elements in inputNumbers");
        else {
            int firstDimension = findFirstDimension(inputNumbers);// find dimensions of pyramid
            int secondDimension = findSecondDimension(firstDimension);
            int[][] pyramid = new int[firstDimension][secondDimension];
            // sort input List
            inputNumbers.sort((integer, t1) -> {
                if(integer == null)
                    throw new CannotBuildPyramidException("List contains null value");
                else {
                    return integer.compareTo(t1);
                }

            });

            //build every level of the pyramid from the top to the bottom
            int currentLevel = 1;
            int currentPos = 0;
            for(int i=0;i<firstDimension;i++) {
                pyramid[i] = CreateLevel(secondDimension, getNeededNumbers(currentPos, currentLevel, inputNumbers));
                currentPos += currentLevel;
                currentLevel++;
            }
            return pyramid;
        }


    }










}
