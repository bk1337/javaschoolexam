package com.tsystems.javaschool.tasks.pyramid;

public class CannotBuildPyramidException extends RuntimeException {
        private String statement;
        public String getStatement(){
            return statement;
        }
        CannotBuildPyramidException(String statement){
            this.statement = statement;
        }

}
