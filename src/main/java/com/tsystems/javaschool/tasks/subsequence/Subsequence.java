package com.tsystems.javaschool.tasks.subsequence;

import java.util.LinkedList;
import java.util.List;

public class Subsequence {

    /**
     * Checks that symbols that can create equal subsequence go in right order
     *
     * @param positions list of positions already used to build an equal sequence
     * @param position position of an element which can be used to build an equal sequence
     * @return <code>true</code> if new position is ok, otherwise <code>false</code>
     */
    private boolean checkPosition(LinkedList<Integer> positions, int position){
        for (int x: positions) {
            if(x>position)
                return false;
        }
        return true;
    }

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if(x == null || y==null){
            throw new IllegalArgumentException();
        }
        else {
            int counter = 0;
            LinkedList<Integer> positions = new LinkedList<>();
            for(int i=0;i<x.size();i++){
                if(y.contains(x.get(i)) && (positions.size()==0 || checkPosition(positions, y.indexOf(x.get(i))))){
                    counter++;
                    positions.add(y.indexOf(x.get(i)));
                }

                else
                    return false;
            }
            if(counter == x.size())
                return true;
            else
                return false;
        }

    }



}
